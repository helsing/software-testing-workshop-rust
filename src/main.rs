use std::{thread, time};

use multimap::MultiMap;

#[derive(Debug, PartialEq, Eq)]
enum Type {
    Gold,
    Silver,
    Bronze,
}

#[derive(Debug)]
struct Medal {
    r#type: Type,
    country: String,
}

// Determines the current list of (athletics) medals as advertised by olympics.com.
fn fetch_medals() -> Result<Vec<Medal>, Box<dyn std::error::Error>> {
    // From: https://olympics.com/en/olympic-games/tokyo-2020/results/athletics
    let url = "https://helsing.gitlab.io/software-testing-workshop-python/athletics.json";
    let json: serde_json::Value = reqwest::blocking::get(url)?.json()?;
    let mut medals = vec![];
    for event in json["pageProps"]["gameDiscipline"]["events"]
        .as_array()
        .unwrap()
    {
        for award in event["awards"].as_array().unwrap() {
            let r#type = match award["medalType"].as_str().unwrap() {
                "GOLD" => Type::Gold,
                "SILVER" => Type::Silver,
                "BRONZE" => Type::Bronze,
                _ => panic!(),
            };
            let country = if !award["participant"]["countryObject"].is_object() {
                award["participant"]["title"].as_str().unwrap()
            } else {
                award["participant"]["countryObject"]["name"]
                    .as_str()
                    .unwrap()
            };
            let medal = Medal {
                r#type,
                country: country.to_string(),
            };
            medals.push(medal);
        }
    }
    Ok(medals)
}

// Returns a list of (country, #gold, #silver, #bronze) tuples
// ranked by medal count.
fn create_table(medals: &Vec<Medal>) -> Vec<(String, usize, usize, usize)> {
    // Collect all medals a country has won
    let mut by_country = MultiMap::new();
    for medal in medals {
        by_country.insert(medal.country.clone(), medal);
    }

    // Collect the number of gold/silver/bronze for each country
    let mut countries = vec![];
    for (country, country_medals) in by_country.iter_all() {
        countries.push((
            country.into(),
            country_medals
                .iter()
                .filter(|x| x.r#type == Type::Gold)
                .count(),
            country_medals
                .iter()
                .filter(|x| x.r#type == Type::Silver)
                .count(),
            country_medals
                .iter()
                .filter(|x| x.r#type == Type::Bronze)
                .count(),
        ));
    }

    // Sort by reverse gold/silver/bronze medal count
    countries.sort_by_key(|elem| (elem.1, elem.2, elem.3));
    countries.into_iter().rev().collect()
}

fn main() {
    let mut last_top5 = None;

    loop {
        let medals = fetch_medals().unwrap();
        let table = create_table(&medals);
        let top5: Option<Vec<String>> = Some(table.iter().take(5).map(|e| e.0.clone()).collect());
        if top5 != last_top5 {
            println!("{:#?}", &top5);
        }
        last_top5 = top5;
        thread::sleep(time::Duration::from_secs(2));
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn test_dummy() {
        assert_eq!(1, 1);
    }
}
